import {RouterModule, Routes} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";

const defaultRoute: Routes = [
    {
        path: "",
        loadChildren: "./class-schema/class-schema.module#ClassSchemaModule"
    }
];

const classSchemaRoutes: Routes = [
    {
        path: "class-schema",
        loadChildren: "./class-schema/class-schema.module#ClassSchemaModule"
    }
];

const testClassRoutes: Routes = [
    {
        path: "test-class",
        loadChildren: "./test-class/test-class.module#TestClassModule"
    }
];

const appRoutes: Routes = [
    ...classSchemaRoutes,
    ...testClassRoutes,
    ...defaultRoute
];

export const rootRoutes: ModuleWithProviders = RouterModule.forRoot(appRoutes);