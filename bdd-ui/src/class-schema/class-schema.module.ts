import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ClassSchemaListComponent} from "./components/class-schema-list.component";
import {ClassSchemaService} from "./services/class-schema.service";
import {classSchemaRoutes} from "./class-schema.routing";
import {ClassSchemaDetailsComponent} from "./components/class-schema-details.component";

@NgModule({
    imports: [CommonModule, HttpClientModule, FormsModule, ReactiveFormsModule, classSchemaRoutes],
    exports: [ClassSchemaListComponent],
    declarations: [ClassSchemaListComponent, ClassSchemaDetailsComponent],
    providers: [ClassSchemaService]
})
export class ClassSchemaModule {

}