import {RouterModule, Routes} from "@angular/router";
import {ClassSchemaListComponent} from "./components/class-schema-list.component";
import {ModuleWithProviders} from "@angular/core";
import {ClassSchemaDetailsComponent} from "./components/class-schema-details.component";

const classSchemaRoutesConfig: Routes = [
    {
        path: "",
        component: ClassSchemaListComponent
    },
    {
        path: ":fullyQualifiedClassName",
        component: ClassSchemaDetailsComponent
    }
];
export const classSchemaRoutes: ModuleWithProviders = RouterModule.forChild(classSchemaRoutesConfig);