import {Component, OnInit} from "@angular/core";
import {ClassSchemaService} from "../services/class-schema.service";
import {ClassSchema} from "../models/class-schema";

@Component({
    selector: "class-schema-list",
    templateUrl: "../views/class-schema-list.component.html"
})
export class ClassSchemaListComponent implements OnInit {

    classSchemas: ClassSchema[] = [];

    constructor(private _classSchemaService: ClassSchemaService) {
    }

    ngOnInit(): void {
        this.loadClassSchema();
    }

    loadClassSchema(): void {
        this._classSchemaService.getAllClassSchemas().subscribe(
            data => this.classSchemas = data,
            error => console.log(error),
            () => console.log("Service call successful")
        );
    }

    viewMethodDetails(fullyQualifiedClassName: string): void {
        console.log(fullyQualifiedClassName);
    }
}