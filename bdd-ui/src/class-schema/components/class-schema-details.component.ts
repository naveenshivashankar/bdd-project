import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {ClassSchemaService} from "../services/class-schema.service";
import {ClassSchema} from "../models/class-schema";

@Component({
    selector: "class-schema-details",
    templateUrl: "../views/class-schema-details.component.html"
})
export class ClassSchemaDetailsComponent implements OnInit {

    classSchema: ClassSchema;

    constructor(private _activatedRoute: ActivatedRoute, private _classSchemaService: ClassSchemaService) {
    }

    ngOnInit(): void {
        this._activatedRoute.params.subscribe(
            data => {
                let fullyQualifiedClassName = data["fullyQualifiedClassName"];
                this._classSchemaService.getClassSchemaDetails(fullyQualifiedClassName).subscribe(
                    data => this.classSchema = data,
                    err => console.error(err),
                    () => console.log("Service Call Successful")
                );
            }
        );
    }
}