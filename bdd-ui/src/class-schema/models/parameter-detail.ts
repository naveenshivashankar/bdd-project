export class ParameterDetail {
    parameterClassName: string;
    parameterClassPackageName: string;
    fullyQualifiedParameterClassName: string;
    primitive: boolean;
}