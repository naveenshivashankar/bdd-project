export class ExceptionDetail {
    exceptionClassName: string;
    exceptionClassPackageName: string;
    fullyQualifiedExceptionClassName: string;
}