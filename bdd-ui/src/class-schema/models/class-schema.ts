import {MethodDetail} from "./method-details";

export class ClassSchema {
    fullyQualifiedClassName: string;
    packageName: string;
    className: string;
    methods: MethodDetail[];
    innerClasses: ClassSchema[];
}