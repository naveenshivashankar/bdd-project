import {ParameterDetail} from "./parameter-detail";
import {ReturnTypeDetail} from "./return-type-detail";
import {ExceptionDetail} from "./exception-detail";

export class MethodDetail {
    objectId: string;
    methodIndex: number;
    methodName: string;
    parameterCount: number;
    parameters: ParameterDetail[];
    returnType: ReturnTypeDetail;
    exceptions: ExceptionDetail[];
}