export class ReturnTypeDetail {
    methodReturnClassName: string;
    methodReturnClassPackageName: string;
    fullyQualifiedReturnClassName: string;
    primitive: boolean;
}