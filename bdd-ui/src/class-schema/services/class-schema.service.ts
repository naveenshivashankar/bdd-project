import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ClassSchema} from "../models/class-schema";

@Injectable()
export class ClassSchemaService {

    constructor(private _http: HttpClient) {
    }

    getAllClassSchemas(): Observable<ClassSchema[]> {
        return this._http.get<ClassSchema[]>("http://localhost:8080/api/classes");
    }

    getClassSchemaDetails(fullyQualifiedCLassName:string): Observable<ClassSchema> {
        return this._http.get<ClassSchema>(`http://localhost:8080/api/classes/${fullyQualifiedCLassName}`);
    }
}