export class MethodParameter {
    index: number;
    primitive: boolean;
    paramFullyQualifiedClassName: string;
    paramClassName: string;
    data: string;
}