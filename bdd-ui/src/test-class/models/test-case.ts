import {MethodParameter} from "./method-parameter";
import {ExpectedValue} from "./expected-value";

export class TestCase {
    assertionMethod: string;
    assertionMessage: string;
    testCaseMethodName: string;
    methodName: string;
    methodParameters: MethodParameter[];
    expectedValue: ExpectedValue;
}