export class ExpectedValue {
    primitive: boolean;
    paramFullyQualifiedClassName: string;
    data: string;
}