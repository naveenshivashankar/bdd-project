import {FormControl, FormGroup, Validators} from "@angular/forms";

export class TestCaseForm {
    newTestCase: FormGroup = new FormGroup({
        fullQualifiedClassName: new FormControl("", [Validators.required]),
        testClassName: new FormControl("", [Validators.required]),
        testCases: new FormGroup({
            assertionMethod: new FormControl("", [Validators.required]),
            assertionMessage: new FormControl("", [Validators.required]),
            testCaseMethodName: new FormControl("", [Validators.required]),
            methodName: new FormControl("", [Validators.required]),
            methodParameters: new FormGroup({
                index: new FormControl("", [Validators.required]),
                primitive: new FormControl("", [Validators.required]),
                paramFullyQualifiedClassName: new FormControl("", [Validators.required]),
                paramClassName: new FormControl("", [Validators.required]),
                data: new FormControl("", [Validators.required])
            }),
            expectedValue: new FormGroup({
                primitive: new FormControl("", [Validators.required]),
                paramFullyQualifiedClassName: new FormControl("", [Validators.required]),
                data: new FormControl("", [Validators.required])
            })
        })
    });
}