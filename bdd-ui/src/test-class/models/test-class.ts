import {TestCase} from "./test-case";

export class TestClass {
    fullQualifiedClassName: string;
    testClassName: string;
    testCases: TestCase[];
}