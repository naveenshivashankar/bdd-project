import {NgModule} from "@angular/core";
import {TestClassService} from "./services/test-class.service";
import {NewTestClassComponent} from "./components/new-test-class.component";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {testClassRoutes} from "./test-class.routing";

@NgModule({
    imports: [CommonModule, RouterModule, FormsModule, ReactiveFormsModule, testClassRoutes],
    exports: [],
    declarations: [NewTestClassComponent],
    providers: [TestClassService]
})
export class TestClassModule {
}