import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TestCaseForm} from "../models/test-case-form";

@Component({
    selector: "new-test-class",
    templateUrl: "../views/new-test-class.component.html"
})
export class NewTestClassComponent implements OnInit {
    methodObjectId: string;

    fullyQualifiedClassName: string;

    testCase: TestCaseForm = new TestCaseForm();

    constructor(private _activatedRoute: ActivatedRoute) {

    }

    ngOnInit(): void {
        this._activatedRoute.params.subscribe(
            params => {
                this.fullyQualifiedClassName = params["fullyQualifiedClassName"];
                this.methodObjectId = params["methodObjectId"];
            }
        );
    }
}