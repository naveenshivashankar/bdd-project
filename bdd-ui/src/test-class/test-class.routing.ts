import {RouterModule, Routes} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";
import {NewTestClassComponent} from "./components/new-test-class.component";

const testClassRoutesConfig: Routes = [
    {
        path: ":fullyQualifiedClassName/:methodObjectId/new",
        component: NewTestClassComponent
    }
];
export const testClassRoutes: ModuleWithProviders = RouterModule.forChild(testClassRoutesConfig);