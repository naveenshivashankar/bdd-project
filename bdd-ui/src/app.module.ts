import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./app.component";
import {rootRoutes} from "./app.routing";
import {HomeModule} from "./home/home.module";

@NgModule({
    imports: [BrowserModule, HomeModule, rootRoutes], //Angular in-built modules or custom modules
    exports: [],
    declarations: [AppComponent],//Components, Pipes and Directives
    providers: [], //Services
    bootstrap: [AppComponent]//Root Component
})
export class AppModule {

}