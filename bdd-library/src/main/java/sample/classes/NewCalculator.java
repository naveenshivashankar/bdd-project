package sample.classes;

public class NewCalculator extends Calculator {
    public int sub(Input input) {
        return input.getNum1() - input.getNum2();
    }

    private  int div(Input input) {
        return input.getNum1() / input.getNum2();
    }
}
