package com.synechron.bddlibrary.controller;

import com.synechron.bddlibrary.model.ClassSchema;
import com.synechron.bddlibrary.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/classes")
public class ClassController {

    @Autowired
    ClassService classService;

    @GetMapping
    public List<ClassSchema> getAllClasses() {
        return classService.getAllClasses();
    }

    @GetMapping("/{fullyQualifiedClassName}")
    public ClassSchema getClassSchema(@PathVariable("fullyQualifiedClassName") String fullyQualifiedClassName){
        return classService.getClassSchema(fullyQualifiedClassName);
    }
}
