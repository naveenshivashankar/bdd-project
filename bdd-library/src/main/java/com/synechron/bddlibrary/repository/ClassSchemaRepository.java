package com.synechron.bddlibrary.repository;

import com.synechron.bddlibrary.model.ClassSchema;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassSchemaRepository extends MongoRepository<ClassSchema, String> {
    List<ClassSchema> findDistinctFirstByFullyQualifiedClassName();

    ClassSchema findByFullyQualifiedClassName(String fullyQualifiedClassName);
}
