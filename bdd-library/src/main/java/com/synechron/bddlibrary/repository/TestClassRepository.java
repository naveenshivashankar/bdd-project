package com.synechron.bddlibrary.repository;

import com.synechron.bddlibrary.model.TestClass;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestClassRepository extends MongoRepository<TestClass, ObjectId> {

    TestClass findByFullQualifiedClassName(String fullQualifiedClassName);
}
