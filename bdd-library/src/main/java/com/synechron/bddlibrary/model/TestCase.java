package com.synechron.bddlibrary.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class TestCase {
    private AssertEnum assertionMethod;
    private String assertionMessage;
    private String testCaseMethodName;
    private String methodName;
    private int parameterCount;
    private MethodParameter[] methodParameters;
    private ExpectedValue expectedValue;

    public AssertEnum getAssertionMethod() {
        return assertionMethod;
    }

    public void setAssertionMethod(AssertEnum assertionMethod) {
        this.assertionMethod = assertionMethod;
    }

    public String getAssertionMessage() {
        return assertionMessage;
    }

    public void setAssertionMessage(String assertionMessage) {
        this.assertionMessage = assertionMessage;
    }

    public String getTestCaseMethodName() {
        return testCaseMethodName;
    }

    public void setTestCaseMethodName(String testCaseMethodName) {
        this.testCaseMethodName = testCaseMethodName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public int getParameterCount() {
        return parameterCount;
    }

    public void setParameterCount(int parameterCount) {
        this.parameterCount = parameterCount;
    }

    public MethodParameter[] getMethodParameters() {
        return methodParameters;
    }

    public void setMethodParameters(MethodParameter[] methodParameters) {
        this.methodParameters = methodParameters;
    }

    public ExpectedValue getExpectedValue() {
        return expectedValue;
    }

    public void setExpectedValue(ExpectedValue expectedValue) {
        this.expectedValue = expectedValue;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
