package com.synechron.bddlibrary.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class ParameterDetail {

    private String parameterClassName;

    private String parameterClassPackageName;

    private String fullyQualifiedParameterClassName;

    private boolean primitive = true;

    public String getParameterClassName() {
        return parameterClassName;
    }

    public void setParameterClassName(String parameterClassName) {
        this.parameterClassName = parameterClassName;
    }

    public String getParameterClassPackageName() {
        return parameterClassPackageName;
    }

    public void setParameterClassPackageName(String parameterClassPackageName) {
        this.parameterClassPackageName = parameterClassPackageName;
        setPrimitive(false);
    }

    public String getFullyQualifiedParameterClassName() {
        return fullyQualifiedParameterClassName;
    }

    public void setFullyQualifiedParameterClassName(String fullyQualifiedParameterClassName) {
        this.fullyQualifiedParameterClassName = fullyQualifiedParameterClassName;
    }

    public boolean isPrimitive() {
        return primitive;
    }

    public void setPrimitive(boolean primitive) {
        this.primitive = primitive;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
