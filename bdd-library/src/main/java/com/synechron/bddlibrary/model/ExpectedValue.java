package com.synechron.bddlibrary.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class ExpectedValue {

    private boolean primitive;

    private String className;

    private String paramFullyQualifiedClassName;

    private String data;

    public boolean isPrimitive() {
        return primitive;
    }

    public void setPrimitive(boolean primitive) {
        this.primitive = primitive;
    }

    public String getParamFullyQualifiedClassName() {
        return paramFullyQualifiedClassName;
    }

    public void setParamFullyQualifiedClassName(String paramFullyQualifiedClassName) {
        this.paramFullyQualifiedClassName = paramFullyQualifiedClassName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
