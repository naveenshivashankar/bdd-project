package com.synechron.bddlibrary.model;

public enum AssertEnum {
    assertEquals, assertTrue, assertNull
}
