package com.synechron.bddlibrary.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "ClassSchema")
public class ClassSchema {
    @Id
    private String fullyQualifiedClassName;

    private String packageName;

    private String className;

    @Transient
    private List<MethodDetail> methods;

    private List<ClassSchema> innerClasses;

    public ClassSchema() {
        this.methods = new ArrayList<>();
        this.innerClasses = new ArrayList<>();
    }

    public String getFullyQualifiedClassName() {
        return fullyQualifiedClassName;
    }

    public void setFullyQualifiedClassName(String fullyQualifiedClassName) {
        this.fullyQualifiedClassName = fullyQualifiedClassName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<MethodDetail> getMethods() {
        return methods;
    }

    public void setMethods(List<MethodDetail> methods) {
        this.methods = methods;
    }

    public void addMethod(MethodDetail methodDetail) {
        this.methods.add(methodDetail);
    }

    public List<ClassSchema> getInnerClasses() {
        return this.innerClasses;
    }

    public void setInnerClasses(List<ClassSchema> innerClasses) {
        this.innerClasses = innerClasses;
    }

    public void addInnerClass(ClassSchema innerClass) {
        this.innerClasses.add(innerClass);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
