package com.synechron.bddlibrary.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class ReturnTypeDetail {

    private String methodReturnClassName;

    private String methodReturnClassPackageName;

    private String fullyQualifiedReturnClassName;

    private boolean primitive = true;

    public String getMethodReturnClassName() {
        return methodReturnClassName;
    }

    public void setMethodReturnClassName(String methodReturnClassName) {
        this.methodReturnClassName = methodReturnClassName;
    }

    public String getMethodReturnClassPackageName() {
        return methodReturnClassPackageName;
    }

    public void setMethodReturnClassPackageName(String methodReturnClassPackageName) {
        this.methodReturnClassPackageName = methodReturnClassPackageName;
        setPrimitive(false);
    }

    public boolean isPrimitive() {
        return primitive;
    }

    public void setPrimitive(boolean primitive) {
        this.primitive = primitive;
    }

    public String getFullyQualifiedReturnClassName() {
        return fullyQualifiedReturnClassName;
    }

    public void setFullyQualifiedReturnClassName(String fullyQualifiedReturnClassName) {
        this.fullyQualifiedReturnClassName = fullyQualifiedReturnClassName;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
