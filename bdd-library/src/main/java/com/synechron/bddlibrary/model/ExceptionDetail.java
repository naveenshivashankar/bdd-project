package com.synechron.bddlibrary.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class ExceptionDetail {

    private String exceptionClassName;

    private String exceptionClassPackageName;

    private String fullyQualifiedExceptionClassName;

    public String getExceptionClassName() {
        return exceptionClassName;
    }

    public void setExceptionClassName(String exceptionClassName) {
        this.exceptionClassName = exceptionClassName;
    }

    public String getExceptionClassPackageName() {
        return exceptionClassPackageName;
    }

    public String getFullyQualifiedExceptionClassName() {
        return fullyQualifiedExceptionClassName;
    }

    public void setFullyQualifiedExceptionClassName(String fullyQualifiedExceptionClassName) {
        this.fullyQualifiedExceptionClassName = fullyQualifiedExceptionClassName;
    }

    public void setExceptionClassPackageName(String exceptionClassPackageName) {
        this.exceptionClassPackageName = exceptionClassPackageName;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
