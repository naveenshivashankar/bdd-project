package com.synechron.bddlibrary.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class MethodParameter {

    private int index;

    private boolean primitive = true;

    private String paramFullyQualifiedClassName;

    private String paramClassName;

    private String data;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isPrimitive() {
        return primitive;
    }

    public void setPrimitive(boolean primitive) {
        this.primitive = primitive;
    }

    public String getParamFullyQualifiedClassName() {
        return paramFullyQualifiedClassName;
    }

    public void setParamFullyQualifiedClassName(String paramFullyQualifiedClassName) {
        this.paramFullyQualifiedClassName = paramFullyQualifiedClassName;
        setPrimitive(false);
    }

    public String getParamClassName() {
        return paramClassName;
    }

    public void setParamClassName(String paramClassName) {
        this.paramClassName = paramClassName;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
