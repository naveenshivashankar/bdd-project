package com.synechron.bddlibrary.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "MethodDetail")
public class MethodDetail {

    @Id
    private ObjectId objectId;

    private String fullyQualifiedClassName;

    private int methodIndex;

    private String methodName;

    private int parameterCount;

    private ParameterDetail[] parameters;

    private ReturnTypeDetail returnType;

    private ExceptionDetail[] exceptions;

    public MethodDetail() {
    }

    public MethodDetail(int index, int parameterCount) {
        this.objectId = ObjectId.get();
        this.methodIndex = index;
        this.parameterCount = parameterCount;
        if (this.parameterCount > 0) {
            parameters = new ParameterDetail[this.parameterCount];
        }
    }

    public String getObjectId() {
        return objectId == null ? "" : objectId.toString();
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public int getParameterCount() {
        return parameterCount;
    }

    public void setParameterCount(int parameterCount) {
        this.parameterCount = parameterCount;
    }

    public ParameterDetail[] getParameters() {
        return parameters;
    }

    public void setParameters(ParameterDetail[] parameters) {
        this.parameters = parameters;
    }

    public ReturnTypeDetail getReturnType() {
        return returnType;
    }

    public void setReturnType(ReturnTypeDetail returnType) {
        this.returnType = returnType;
    }

    public ExceptionDetail[] getExceptions() {
        return exceptions;
    }

    public void setExceptions(ExceptionDetail[] exceptions) {
        this.exceptions = exceptions;
    }

    public int getMethodIndex() {
        return methodIndex;
    }

    public void setMethodIndex(int methodIndex) {
        this.methodIndex = methodIndex;
    }

    public String getFullyQualifiedClassName() {
        return fullyQualifiedClassName;
    }

    public void setFullyQualifiedClassName(String fullyQualifiedClassName) {
        this.fullyQualifiedClassName = fullyQualifiedClassName;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
