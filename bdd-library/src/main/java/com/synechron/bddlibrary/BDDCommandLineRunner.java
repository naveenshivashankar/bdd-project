package com.synechron.bddlibrary;

import com.synechron.bddlibrary.model.ClassSchema;
import com.synechron.bddlibrary.model.TestClass;
import com.synechron.bddlibrary.service.ClassService;
import com.synechron.bddlibrary.service.TestClassService;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.StringWriter;

@Component
public class BDDCommandLineRunner implements CommandLineRunner {
    @Autowired
    ClassService classService;

    @Autowired
    TestClassService testClassService;

    @Override
    public void run(String... args) throws Exception {
        generateTestClass("sample.classes.NewCalculator");
    }

    private void generateTestClass(String fullyQualifiedClassName) {
        //updateClassSchema(fullyQualifiedClassName);
        //ClassSchema classSchema = classService.getClassSchema(fullyQualifiedClassName);
        //testClassService.populateTestClassData(classSchema);
        //TestClass testClass = getTestClassData(fullyQualifiedClassName + "Test");
        TestClass testClass = getTestClassData("sample.classes.CalculatorTest");
        generateTestClass(testClass);
    }

    private TestClass getTestClassData(String fullyQualifiedClassName) {
        return testClassService.getTestClassData(fullyQualifiedClassName);
    }

    private ClassSchema updateClassSchema(String fullyQualifiedClassName) {
        return classService.updateClassSchema(fullyQualifiedClassName);
    }

    private void generateTestClass(TestClass testClass) {
        try {
            VelocityEngine ve = new VelocityEngine();
            ve.init();

            Template template = ve.getTemplate("new-class-template.vm");
            StringWriter sw = new StringWriter();

            VelocityContext context = new VelocityContext();
            context.put("data", testClass);
            template.merge(context, sw);
            //System.out.println(sw.toString());
            writeToFile(testClass, sw.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void writeToFile(TestClass testClass, String data) {
        String testDir = null;
        String outDir = null;
        File outFile = null;
        OutputStream os = null;

        try {
            outFile = new File("");
            testDir = outFile.getAbsolutePath() + "/bdd-library/src/test/java/";
            outDir = testDir + testClass.getPackageName().replaceAll("\\.", "/");

            outFile = new File(outDir);
            outFile.mkdirs();
            outFile = new File(outDir + "/"+ testClass.getTestClassName() + ".java");

            os = new FileOutputStream(outFile);
            os.write(data.getBytes(), 0, data.length());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
