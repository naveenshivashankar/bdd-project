package com.synechron.bddlibrary.service;

import com.synechron.bddlibrary.model.ClassSchema;
import com.synechron.bddlibrary.model.TestClass;

public interface TestClassService {

    TestClass getTestClassData(String fullyQualifiedClassName);

    TestClass populateTestClassData(ClassSchema classDetail);
}
