package com.synechron.bddlibrary.service;

import com.synechron.bddlibrary.model.ClassSchema;

import java.util.List;

public interface ClassService {

    List<ClassSchema> getAllClasses();

    ClassSchema getClassSchema(String fullyQualifiedClassName);

    ClassSchema getClassDetails(String fullyQualifiedClassName);

    ClassSchema updateClassSchema(String fullyQualifiedClassName);
}
