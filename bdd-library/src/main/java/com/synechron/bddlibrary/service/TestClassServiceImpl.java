package com.synechron.bddlibrary.service;

import com.synechron.bddlibrary.model.*;
import com.synechron.bddlibrary.repository.TestClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class TestClassServiceImpl implements TestClassService {

    @Autowired
    TestClassRepository testClassRepository;

    @Override
    public TestClass getTestClassData(String fullyQualifiedClassName){
        TestClass testClass = testClassRepository.findByFullQualifiedClassName(fullyQualifiedClassName);
        testClass.getImports().add("junit.framework.TestCase");
        testClass.getImports().add("org.junit.Test");
        testClass.getImports().add("com.fasterxml.jackson.databind.ObjectMapper");
        return testClass;
    }

    //@Override
    public TestClass loadTestClassData(ClassSchema classDetail) {
        return populateTestClassData(classDetail);
    }

    @Override
    public TestClass populateTestClassData(ClassSchema classDetail) {
        TestClass testClassData = new TestClass();

        testClassData.setPackageName(classDetail.getPackageName());
        testClassData.setTestClassName(classDetail.getClassName() + "Test");
        testClassData.setClassName(classDetail.getClassName());
        testClassData.setFullQualifiedClassName(testClassData.getPackageName() + "." + testClassData.getTestClassName());
        populateTestCasesData(classDetail, testClassData);
        testClassRepository.save(testClassData);
        return testClassData;
    }

    private void populateTestCasesData(ClassSchema classDetail, TestClass testClassData) {
        MethodDetail methodDetails = null;

        for (int i = 0; i < classDetail.getMethods().size(); i++) {
            methodDetails = classDetail.getMethods().get(i);
            testClassData.addTestCase(createTestCase(methodDetails));
        }
    }

    private TestCase createTestCase(MethodDetail methodDetail) {
        TestCase testCase = null;
        MethodParameter[] methodParameters = null;
        MethodParameter methodParameter = null;
        ExpectedValue expectedValue = null;

        testCase = new TestCase();

        testCase.setMethodName(methodDetail.getMethodName());
        testCase.setAssertionMethod(AssertEnum.assertEquals);
        testCase.setTestCaseMethodName("test" + testCase.getMethodName().substring(0, 1).toUpperCase() + testCase.getMethodName().substring(1));
        if (methodDetail.getParameterCount() > 0) {
            methodParameters = new MethodParameter[methodDetail.getParameterCount()];
            methodParameter = new MethodParameter();
            methodParameter.setIndex(1);
            methodParameter.setParamFullyQualifiedClassName(methodDetail.getParameters()[0].getFullyQualifiedParameterClassName());
            methodParameter.setParamClassName(methodDetail.getParameters()[0].getParameterClassName());
            methodParameter.setData("{num1:5, num2:10}");
            methodParameters[0] = methodParameter;
            testCase.setMethodParameters(methodParameters);
        }

        expectedValue = new ExpectedValue();
        expectedValue.setPrimitive(true);
        expectedValue.setData("15");
        testCase.setExpectedValue(expectedValue);

        return testCase;
    }
}
