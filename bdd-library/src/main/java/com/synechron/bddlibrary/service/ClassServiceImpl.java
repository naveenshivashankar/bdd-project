package com.synechron.bddlibrary.service;

import com.synechron.bddlibrary.model.*;
import com.synechron.bddlibrary.repository.ClassSchemaRepository;
import com.synechron.bddlibrary.repository.MethodDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;

@Service
public class ClassServiceImpl implements ClassService {

    @Autowired
    ClassSchemaRepository classSchemaRepository;

    @Autowired
    MethodDetailRepository methodDetailRepository;

    @Override
    public List<ClassSchema> getAllClasses() {
        return classSchemaRepository.findAll();
    }

    @Override
    public ClassSchema getClassSchema(String fullyQualifiedClassName){
        ClassSchema classSchema = null;

        classSchema = classSchemaRepository.findByFullyQualifiedClassName(fullyQualifiedClassName);
        classSchema.setMethods(methodDetailRepository.findByFullyQualifiedClassName(fullyQualifiedClassName));
        return classSchema;
    }

    public ClassSchema getClassDetails(String fullyQualifiedClassName) {
        Class currClass = null;
        ClassSchema classDetails = null;

        try {
            currClass = Class.forName(fullyQualifiedClassName);
            if (!currClass.isInterface() && !Modifier.isAbstract(currClass.getModifiers())) {
                classDetails = new ClassSchema();

                classDetails.setFullyQualifiedClassName(currClass.getCanonicalName());
                populateClassPackageName(currClass, classDetails);
                populateClassName(currClass, classDetails);
                populateClassMethodsDetails(currClass, classDetails);
                populateInnerClassDetails(currClass, classDetails);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return classDetails;
    }

    @Override
    public ClassSchema updateClassSchema(String fullyQualifiedClassName) {
        ClassSchema classSchema = null;

        classSchema = getClassDetails(fullyQualifiedClassName);
        //classSchemaRepository.save(classSchema);
        //methodDetailRepository.saveAll(classSchema.getMethods());
        return classSchema;
    }

    private void populateClassPackageName(Class currClass, ClassSchema classDetails) {
        classDetails.setPackageName(getPackageName(currClass));
    }

    private void populateClassName(Class currClass, ClassSchema classDetails) {
        classDetails.setClassName(currClass.getSimpleName());
    }

    private void populateClassMethodsDetails(Class currClass, ClassSchema classDetails) {
        Method method = null;
        for (int i = 0; i < currClass.getDeclaredMethods().length; i++) {
            method = currClass.getDeclaredMethods()[i];
            if (!Modifier.isAbstract(method.getModifiers()) && method.getDeclaringClass() != Object.class) {
                System.out.println(method.getName());
                classDetails.addMethod(getMethodDetails(i, method));
            }
        }
        for (int i = 0; i < currClass.getSuperclass().getMethods().length; i++) {
            method = currClass.getSuperclass().getMethods()[i];
            if (!Modifier.isAbstract(method.getModifiers()) && method.getDeclaringClass() != Object.class) {
                System.out.println(method.getName());
                classDetails.addMethod(getMethodDetails(i, method));
            }
        }
    }

    private void populateInnerClassDetails(Class currClass, ClassSchema classDetails) {
        for (Class innerClass : currClass.getDeclaredClasses()) {
            classDetails.addInnerClass(getClassDetails(innerClass.getName()));
        }
    }

    private MethodDetail getMethodDetails(int index, Method method) {
        MethodDetail methodDetail = null;

        methodDetail = new MethodDetail(index, method.getParameterCount());
        methodDetail.setFullyQualifiedClassName(method.getDeclaringClass().getName());
        methodDetail.setMethodName(method.getName());
        methodDetail.setParameters(getMethodParameterDetails(method));
        methodDetail.setReturnType(getReturnTypeDetails(method));
        methodDetail.setExceptions(getMethodExceptionDetails(method));
        return methodDetail;
    }

    private ParameterDetail[] getMethodParameterDetails(Method method) {
        ParameterDetail[] parameterDetails = null;

        parameterDetails = new ParameterDetail[method.getParameterCount()];
        for (int i = 0; i < parameterDetails.length; i++) {
            parameterDetails[i] = getParameterDetails(method.getParameterTypes()[i]);
        }
        return parameterDetails;
    }

    private ParameterDetail getParameterDetails(Class paramClass) {
        ParameterDetail paramDetails = null;

        paramDetails = new ParameterDetail();
        paramDetails.setParameterClassName(paramClass.getSimpleName());
        if (!paramClass.isPrimitive()) {
            paramDetails.setParameterClassPackageName(getPackageName(paramClass));
            paramDetails.setFullyQualifiedParameterClassName(paramClass.getCanonicalName());
        }
        return paramDetails;
    }

    private ReturnTypeDetail getReturnTypeDetails(Method method) {
        ReturnTypeDetail returnTypeDetail;

        returnTypeDetail = new ReturnTypeDetail();
        returnTypeDetail.setMethodReturnClassName(method.getReturnType().getSimpleName());
        if (!method.getReturnType().isPrimitive()) {
            returnTypeDetail.setMethodReturnClassPackageName(getPackageName(method.getReturnType()));
            returnTypeDetail.setFullyQualifiedReturnClassName(method.getReturnType().getCanonicalName());
        }
        return returnTypeDetail;
    }

    private ExceptionDetail[] getMethodExceptionDetails(Method method) {
        ExceptionDetail[] exceptionDetails = null;

        exceptionDetails = new ExceptionDetail[method.getExceptionTypes().length];
        for (int i = 0; i < exceptionDetails.length; i++) {
            exceptionDetails[i] = getExceptionDetails(method.getExceptionTypes()[i]);
        }
        return exceptionDetails;
    }

    private ExceptionDetail getExceptionDetails(Class exceptionClass) {
        ExceptionDetail exceptionDetail = null;

        exceptionDetail = new ExceptionDetail();
        exceptionDetail.setExceptionClassName(exceptionClass.getSimpleName());
        if (!exceptionClass.isPrimitive()) {
            exceptionDetail.setExceptionClassPackageName(exceptionClass.getPackage().getName());
            exceptionDetail.setExceptionClassPackageName(getPackageName(exceptionClass));
            exceptionDetail.setFullyQualifiedExceptionClassName(exceptionClass.getCanonicalName());
        }
        return exceptionDetail;
    }

    private String getPackageName(Class currClass) {
        return currClass.isArray() ? (currClass.getComponentType().isPrimitive() ? "" : currClass.getComponentType().getPackage().getName()) : (currClass.isPrimitive() ? "" : currClass.getPackage().getName());
    }
}
